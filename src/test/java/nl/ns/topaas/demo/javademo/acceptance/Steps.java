package nl.ns.topaas.demo.javademo.acceptance;

import cucumber.api.java8.En;

public class Steps implements En {
    public Steps() {
        Given("^everything works so far$", () -> {
            System.out.println("Everything all ok so far!");
        });

        When("^I run this test$", () -> {
            System.out.println("Test is running!");
        });

        Then("^everything is awesome$", () -> {
            System.out.println("And EVERYTHING is awesome");
        });
    }
}