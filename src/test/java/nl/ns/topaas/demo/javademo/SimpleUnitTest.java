package nl.ns.topaas.demo.javademo;

import org.junit.Assert;
import org.junit.Test;

public class SimpleUnitTest{

    @Test
    public void doesThisWork(){
        Assert.assertEquals("yes it does", true, true);
    }
     @Test
    public void doesThisAlsoWork(){
        Assert.assertEquals("yes it does", false,false);
    }

     @Test
    public void whatAboutNumbers(){
        Assert.assertEquals("yes it does", 0, 0);
    }
}